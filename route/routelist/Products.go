package routelist

import (
	"example-products/api"

	"github.com/labstack/echo/v4"
)

func ProductsRoute(echo *echo.Group) {
	echo.POST("", api.ApiProducts)
}
