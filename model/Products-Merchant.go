package model

import (
	"time"

	"gorm.io/datatypes"
)

type ProductsMerchants struct {
	IdPpmse            int            `json:"id_ppmse" gorm:"column:id_ppmse"`
	ProductName        string         `json:"product_name" gorm:"column:product_name"`
	ProductPrice       int64          `json:"product_price" gorm:"column:product_price"`
	IsAvailable        bool           `json:"is_available" gorm:"column:is_available"`
	ProductImages      datatypes.JSON `json:"product_images" gorm:"column:product_images"`
	ProductThumbnail   string         `json:"product_thumbnail" gorm:"column:product_thumbnail"`
	ProductDescription string         `json:"product_description" gorm:"column:product_description"`
	ProductCategory    datatypes.JSON `json:"product_category" gorm:"column:product_category"`
	ProductWeight      int64          `json:"product_weight" gorm:"column:product_weight"`
	Tags               datatypes.JSON `json:"tags" gorm:"column:tags"`
	IsPDN              bool           `json:"is_PDN" gorm:"column:is_PDN"`
	IsTaxable          bool           `json:"is_taxable" gorm:"column:is_taxable"`
	SKU                string         `json:"SKU" gorm:"column:SKU"`
	Score              *float64       `json:"score" gorm:"column:score"`
	MerchantName       string         `json:"merchant_name" gorm:"column:merchant_name"`
	MerchantProvince   string         `json:"merchant_province" gorm:"column:merchant_province"`
	MerchantCity       string         `json:"merchant_city" gorm:"column:merchant_city"`
	MerchantNpwp       string         `json:"merchant_npwp" gorm:"column:merchant_npwp"`
	MerchantNik        string         `json:"merchant_nik" gorm:"column:merchant_nik"`
	IsPKP              bool           `json:"is_PKP" gorm:"column:is_PKP"`
	MerchantScore      float64        `json:"merchant_score" gorm:"column:merchant_score"`
	MaxMerchantScore   float64        `json:"max_merchant_score" gorm:"column:max_merchant_score"`
	MerchantType       string         `json:"merchant_type" gorm:"column:merchant_type"`
	MerchantId         string         `json:"merchant_id" gorm:"column:merchant_id"`
	CreatedAt          *time.Time     `json:"created_at" gorm:"column:created_at"`
	UpdatedAt          *time.Time     `json:"updated_at" gorm:"column:updated_at"`
}

type RequestProducts struct {
	PerPage int `json:"per_page" form:"per_page" query:"per_page" validate:"required"`
	Page    int `json:"page" form:"page" query:"page" validate:"required"`
}
