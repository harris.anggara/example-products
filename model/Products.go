package model

import (
	"time"

	"gorm.io/datatypes"
)

type Products struct {
	Id                 int            `json:"id"`
	IdPpmse            int            `json:"id_ppmse"`
	ProductName        string         `json:"product_name"`
	ProductPrice       int64          `json:"product_price"`
	IsAvailable        bool           `json:"is_available"`
	ProductImages      datatypes.JSON `json:"product_images"`
	ProductThumbnail   string         `json:"product_thumbnail"`
	ProductDescription string         `json:"product_description"`
	ProductCategory    datatypes.JSON `json:"product_category"`
	ProductWeight      int64          `json:"product_weight"`
	Tags               datatypes.JSON `json:"tags"`
	IsPdn              bool           `json:"is_pdn"`
	IsTaxable          bool           `json:"is_taxable"`
	Sku                string         `json:"sku"`
	Score              *float64       `json:"score"`
	MerchantId         string         `json:"merchant_id"`
	CreatedAt          *time.Time     `json:"created_at"`
	UpdatedAt          *time.Time     `json:"updated_at"`
}

func (Products) TableName() string {
	return "products"
}
