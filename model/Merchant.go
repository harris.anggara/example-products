package model

import "time"

type Merchants struct {
	Id               int        `json:"id"`
	IdPpmse          int        `json:"id_ppmse"`
	MerchantId       string     `json:"merchant_id"`
	MerchantName     string     `json:"merchant_name"`
	MerchantProvince string     `json:"merchant_province"`
	MerchantCity     string     `json:"merchant_city"`
	MerchantNpwp     string     `json:"merchant_npwp"`
	MerchantNik      string     `json:"merchant_nik"`
	IsPkp            bool       `json:"is_pkp"`
	MerchantScore    float64    `json:"merchant_score"`
	MaxMerchantScore float64    `json:"max_merchant_score"`
	MerchantType     string     `json:"merchant_type"`
	CreatedAt        *time.Time `json:"created_at"`
	UpdatedAt        *time.Time `json:"updated_at"`
}

func (Merchants) TableName() string {
	return "merchants"
}
