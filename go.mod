module example-products

go 1.15

require (
	github.com/ghodss/yaml v1.0.0 // indirect
	github.com/go-playground/universal-translator v0.18.0 // indirect
	github.com/go-playground/validator v9.31.0+incompatible
	github.com/labstack/echo/v4 v4.7.2
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/tkanos/gonfig v0.0.0-20210106201359-53e13348de2f
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
	gorm.io/datatypes v1.0.7
	gorm.io/driver/mysql v1.3.2
	gorm.io/gorm v1.23.6
)
