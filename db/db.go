package db

import (
	"example-products/config"
	"fmt"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var db *gorm.DB
var err error

// Init DB Connection
func Init() {
	configuration := config.GetConfig()
	connect_string := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local", configuration.DB_USERNAME, configuration.DB_PASSWORD, configuration.DB_HOST, configuration.DB_PORT, configuration.DB_NAME)
	db, err = gorm.Open(mysql.Open(connect_string), &gorm.Config{})

	// defer db.Close()
	if err != nil {
		panic("DB Connection Error")
	}

}

// Manager for conn
func Manager() *gorm.DB {
	return db
}
