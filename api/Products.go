package api

import (
	"example-products/db"
	"example-products/model"
	"fmt"
	"net/http"

	"github.com/labstack/echo/v4"
)

func ApiProducts(c echo.Context) error {

	req := new(model.RequestProducts)

	if err := c.Bind(req); err != nil {
		fmt.Println(err)
		return err
	}

	if err := c.Validate(req); err != nil {
		fmt.Println(err)
		return err
	}

	// db2 for total data products
	db2 := db.Manager()
	var totalDataProducts int64

	db := db.Manager()
	products_merchant := []model.ProductsMerchants{}

	db = db.Table("products").Select("products.*, merchants.merchant_name, merchants.merchant_province, merchants.merchant_city, merchants.merchant_npwp, merchants.merchant_nik, merchants.is_pkp, merchants.merchant_score, merchants.max_merchant_score, merchants.merchant_type").Joins("left join merchants on merchants.merchant_id = products.merchant_id")

	rt := db2.Table("products").Select("products.*, merchants.merchant_name, merchants.merchant_province, merchants.merchant_city, merchants.merchant_npwp, merchants.merchant_nik, merchants.is_pkp, merchants.merchant_score, merchants.max_merchant_score, merchants.merchant_type").Joins("left join merchants on merchants.merchant_id = products.merchant_id").Count(&totalDataProducts)
	if rt.Error != nil {
		fmt.Println(rt.Error)
		return c.JSON(http.StatusInternalServerError, rt.Error)
	}

	if req.PerPage > 0 && req.PerPage < 1000 {
		db = db.Limit(req.PerPage).Offset((req.Page - 1) * req.PerPage)
	} else {
		db = db.Limit(1000).Offset((req.Page - 1) * 1000)
	}

	rs := db.Scan(&products_merchant)

	if rs.Error != nil {
		return c.JSON(http.StatusInternalServerError, rs.Error)
	}

	if rs.RowsAffected == 0 {
		return c.JSON(http.StatusNotFound, &map[string]interface{}{
			"status":  false,
			"code":    "404",
			"message": "Data not found",
		})
	}

	// check last page
	var lastPage bool
	if int64(req.PerPage*req.Page) >= totalDataProducts {
		lastPage = true
	} else {
		lastPage = false
	}

	return c.JSON(http.StatusOK, &map[string]interface{}{
		"status":    true,
		"code":      "200",
		"data":      products_merchant,
		"per_page":  req.PerPage,
		"page":      req.Page,
		"total":     totalDataProducts,
		"last_page": lastPage,
	})
}
