# Base Echo with GORM - Postgres

## Pre-installation
```
go mod tidy
```

## Configurations
Copy `config.json.example` to `config.json` as your own config file.
Configuration used gonfig. All configs are declared in `config/config.json`

## Architecture
| Folder | Details |
| --- | ---|
| api | Holds the api endpoints |
| db | Database Initializer and DB manager |
| helper | Commonly used global function |
| route | router setup |
| model | Models|

## Database
Example database structure is on `db-example/example_products.sql`, you can generate your own data.


## Run development mode
`go run server.go`

## Build for production (Linux)
`GOOS=linux GOARCH=amd64 go build -o build/goAppExampleProducts`

## Run production mode
`./build/goAppExampleProducts`


## Endpoint Products
### URL
```
POST localhost:1329/api/products
```
### Body
```
{
    "page": 100,
    "per_page":100 #Max 1000
}
```

### Response Success
For information of fields, please see `https://tokodaring.lkpp.go.id/dokumentasi/docs/panduan-integrasi/produk#penjelasan-parameter-api-produk`
```
{
    "code": "200",
    "data": [
        {
            "id_ppmse": 878,
            "product_name": "Grape",
            "product_price": 10000,
            "is_available": true,
            "product_images": [
                "/pic/pMTfOChOTm.jpeg"
            ],
            "product_thumbnail": "Apple",
            "product_description": "The Information Pane...": {
                "0": "RhnRRzT3DX"
            },
            "product_weight": 46,
            "tags": [
                "GunUfpbQQY", "Apple", "fruit"
            ],
            "is_PDN": false,
            "is_taxable": false,
            "SKU": "DD-AA-1912-N",
            "score": 967.86,
            "merchant_name": "Christina Ryan",
            "merchant_province": "JTlNZxROo1",
            "merchant_city": "Shenzhen",
            "merchant_npwp": "mGEwuIfFmh",
            "merchant_nik": "3NG8RJ3d9U",
            "is_PKP": false,
            "merchant_score": 20.25,
            "max_merchant_score": 312.28,
            "merchant_type": "mikro",
            "merchant_id": "Hu5dJB39gO",
            "created_at": "2005-08-27T17:59:25+07:00",
            "updated_at": "2009-07-03T10:51:20+07:00"
        }
    ],
    "last_page": true,
    "page": 100,
    "per_page": 1,
    "status": true,
    "total": 10000
}
```
### Response Error
Data not found
```
Http status: 404
{
    "code": "404",
    "message": "Data not found",
    "status": false
}
```
Missing required field
```
Http status: 400
{
    "message": "field 'per_page' is required"
}
```



