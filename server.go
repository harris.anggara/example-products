package main

import (
	"example-products/db"
	"example-products/model"
	"example-products/route"
	"fmt"
)

func main() {
	fmt.Println("Version:", model.Version)
	fmt.Println("Build Number:", model.BuildNumber)
	fmt.Println("Build Date:", model.BuildDate)
	fmt.Println("Builder:", model.BuildUser)
	db.Init()
	e := route.Init()

	e.Logger.Fatal(e.Start(":1329"))
}
