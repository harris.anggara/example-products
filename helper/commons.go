package helper

import (
	"fmt"
	"net/http"

	"github.com/go-playground/validator"
	"github.com/labstack/echo/v4"
)

/*-------------------------------------------------
	HELPER FUNCTION
	This is list of global function that used
	commonly. Refactor your function here only if
	needed and gave it a comment about its function!
--------------------------------------------------*/

func CustomError(e *echo.Echo) {
	e.HTTPErrorHandler = func(err error, c echo.Context) {
		report, ok := err.(*echo.HTTPError)
		if !ok {
			report = echo.NewHTTPError(http.StatusBadRequest, err.Error())
		}

		if castedObject, ok := err.(validator.ValidationErrors); ok {
			for _, err := range castedObject {
				switch err.Tag() {
				case "required":
					report.Message = fmt.Sprintf("field '%s' is required",
						err.Field())
				case "email":
					report.Message = fmt.Sprintf("field '%s' is not valid email",
						err.Field())
				case "gte":
					report.Message = fmt.Sprintf("field '%s' value must be greater than '%s'",
						err.Field(), err.Param())
				case "lte":
					report.Message = fmt.Sprintf("field '%s' value must be lower than field  '%s'",
						err.Field(), err.Param())
				}

				break
			}
		}

		c.Logger().Error(report)
		c.JSON(report.Code, report)
	}
}
